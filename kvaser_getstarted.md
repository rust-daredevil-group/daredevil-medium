# Getting started with any Kvaser adapter on Linux:

## Prerequisites

Download the Linux drivers from:  
https://www.kvaser.com/downloads-kvaser/

Install header files and compiler for kernel compilation.  

In Ubuntu: 

```
$ sudo apt-get install build-essential
```

In Fedora:  

```
$ sudo dnf install kernel-devel kernel-headers
```

## Installation

This will install all Kvaser drivers:

```
$ tar xvzf linuxcan.tar.gz  
$ cd linuxcan  
$ make  
$ sudo make install
```
If you're running Secure Boot on your Linux computer you might want to
read this guide instead:  
https://www.kvaser.com/developer-blog/build-install-signed-kvaser-driver-modules/

## Using

Both the Leaf and Blackbird uses the Kvaser leaf module. To check if the correct
module is running, connect the adapter through USB and run
```
$ lsmod | grep ’kvcommon\|leaf’
```
To manually start/stop/restart the module do:
```
$ /usr/sbin/leaf.sh {start|stop|restart}
```
To check which channel your adapter is using or to see if your hardware is
connected successfully run:
```
$ /usr/doc/canlib/examples/listChannels
```
Examples are provided in `/usr/doc/canlib/examples`.
## Troubleshooting  
**Problem**: Adapter won't work after Linux updates.  
**Solution**: Most likely you've updated to a new kernel version. Uninstall the previous Kvaser drivers  
and build them again. Then reinstall.  

**Problem**: Can't build the drivers.  
**Solution**: Make sure that you have the Linux kernel headers installed on your system. Check the readme in the driver source files for more information.