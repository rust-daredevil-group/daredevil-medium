# Getting started with the Avnet Ultra96 dev board
This is a short introduction on how to get the Ultra96 board up and running
with the medium module.  


## Setup (Linux)
If you're starting from scratch on the Ultra96 it might take some time to get
it up and running. You will need

* SD card with PetaLinux on it
* **Optional:** Kernel headers (for Kvaser drivers)

It might be possible to make the following process easier if using the PetaLinux SDK but this
wasn't tested.  
  
### Build kernel headers
To get the kernel headers (if not easily available to you already)
you will need to build it from Linux kernel source. You can get the kernel that
matches your PetaLinux version [here](https://github.com/Xilinx/linux-xlnx).  

Then you will need the current configuration your kernel is running (on the Ultra96). It should be
placed in ``\boot`` or ``\proc`` and named something like ``config-$(uname -r)`` or ``config.gz``.
Place this file in your kernel source and rename it to ``.config``. Build the kernel by running
```
sudo make olddefconfig
```
to use the old config file and default values. You also might need to run
```
sudo make headers_install
```
Then you need to symlink your kernel source to ``/lib/modules/`uname -r` ``. Do
```
ln -s /path/to/kernel/source /lib/modules/`uname -r`/build
ln -s /path/to/kernel/source /lib/modules/`uname -r`/source
```
Now you should be able to build kernel modules (by default the PetaLinux distro 
includes the necessary tools to build).

## Basics
To connect to the board from your terminal you can connect to it's WiFi which
is named something like *Utra96_* or similar. After connecting you can SSH into it
```
ssh root@192.168.2.1
```

