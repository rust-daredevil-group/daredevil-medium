# Daredevil medium module

This is the medium module for the Daredevil project. It's connected to one or more light modules and will receive light module data
from the CAN bus. This data will be propogated through Ethernet to be displayed on a console. 
See [daredevil-visualizer](https://gitlab.com/rust-daredevil-group/daredevil-visualizer).

Currently this implementation only works on a Linux computer with Kvaser's CAN adapters. It can also run in Linux userspace on the Xilinx Zynq UltraScale+™ MPSoC Ultra96 
dev board together with the Kvaser adapters.

New protocols and communication interfaces can be implemented using __'protocol_interface.rs'__ and 
__'communication_interface.rs'__. A communication interface needs to be made for the type of interface you
want to communicate over (e.g Ethernet or Serial). Whereas a protocol interface is the implementation of
the message you want to use on the communication interface (e.g. CAN frames, Ethernet serialized etc.).

The medium currently uses two interfaces for communication and that is Ethernet and CAN. The implementations
of these two can be found in the source directory.

## Prerequisites
* `cargo`
* If running on a Linux device with Kvaser CAN hardware you need to install Kvaser drivers as well as CANlib. 
See [canlib-rs](https://gitlab.com/rust-daredevil-group/lib/canlib-rs) and the [Kvaser guide](https://gitlab.com/rust-daredevil-group/daredevil-medium/blob/master/kvaser_getstarted.md).
* **Optional:** Avnet Ultra96 board. Check the [Ultra96 guide](https://gitlab.com/rust-daredevil-group/daredevil-medium/blob/master/ultra96_gettingstarted.md) on how to get it up and running.  

## Downloading

```
cd /to/your/directory/of/choice
git clone https://gitlab.com/rust-daredevil-group/daredevil-medium.git
```

## Configuration
Setup the correct CAN bus settings,
```rust
// in main.rs 
let can_ch0 : Can<U128, UartSimpleMessage<U128>> = Can::new(); // using UartSimpleMessage as the protocol
let ch0_settings = can::CanBusSettings{...};
can_ch0.init(ch0_settings);
```
then initialize the ip address where the data will be sent to,
```rust
// in main.rs
let eth0 : Ethernet<U128, UartSimpleMessage<U128>> = Ethernet::new();
let ip_addr = String::from("127.0.0.1::7777");
eth0.init_stream(ip_addr);
```
After that just build and run it.

## Building
To build the project in debug mode simply execute the following in any project directory:
```sh
$ cargo build
```

For an optimized version run:
```sh
$ cargo build --release
```

## Running
Running the project can be done via the executable found in `target/debug/daredevil-medium` from the project's root
directory or by running:
```sh
$ cargo run
```

## License
Licensed under either of

- Apache License, Version 2.0 ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
- MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.