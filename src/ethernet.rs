extern crate generic_array;

use crate::communication_interface::*;
use crate::protocol_interface::*;
use crate::ring_buffer::{RingBuffer, RingBufferInterface};
use generic_array::ArrayLength;
use std::
{
        io::{Write,Read},
        net::TcpStream
};

/// # Ethernet struct
/// 
/// Struct for one connection over Ethernet. 
/// 
/// * `send_buffer` - Internal send buffer. Use send() to add message to this buffer. Then tx() to transmit through Ethernet.  
/// * `recieve_buffer` - Internal receive buffer. Use rx() to read on Ethernet to this buffer. Then receive() to get the message.  
/// * `protocol_type` - Specifies what type of protocol to use. E.g. UartSimpleMessage<N>.  
/// * `stream` - Which TcpStream to use. Is wrapped in an Option<TcpStream>.  
pub struct Ethernet<N, T> where 
    N: ArrayLength<u8>, 
    T: ProtocolInterface<N> 
{
    // Hardware independent variables
    send_buffer: RingBuffer<N>,
    //receive_buffer: RingBuffer<N>, 
    protocol_type: T,
    // Hardware dependent variables
    stream : Option<TcpStream>, // Use Option because Stream needs to be emtpy to be initialized later
}

pub trait EthernetTrait {
    /// Initialize the stream to connect on IP address: **addr**.
    fn init_stream(&mut self, addr: String);
    /// Hardware specific recieve on Ethernet connection.
    fn rx(&mut self);
    /// Hardware specific transmit through Ethernet connection.
    fn tx(&mut self);
}

impl<N, T> CommunicationInterface<N, T> for Ethernet<N, T> where N: ArrayLength<u8>, T: ProtocolInterface<N>
{
    fn new() -> Self {
        Ethernet { send_buffer: RingBufferInterface::new(), protocol_type: T::new(), stream: None } // Init to mostly empty variables
    }

    fn send(& mut self, mut p: T) -> bool {
        println!("Ethernet::Send()");
        let buffer : &mut RingBuffer<N> = p.serialize();
        println!("Buffer length: {}", buffer.len());
        for _val in 0..buffer.len() {
            let a = buffer.pop();
            //println!("Adding to Ethernet send buffer: {}", a);
            self.send_buffer.push(a);
        }
        true
    }

    // TODO
    fn receive(& mut self) -> T {
        let a : T = T::new();
        a
    }
}

impl<N, T> EthernetTrait for Ethernet<N, T> where N: ArrayLength<u8>, T: ProtocolInterface<N> {
    
    fn init_stream(&mut self, addr: String) {
        self.stream = Some(TcpStream::connect(addr.to_string()).unwrap());
    }

    // TODO
    fn rx(&mut self) {
        let mut ethernet_read = [0;4];
        if let Some(writer) = &mut self.stream {
            writer.read(&mut ethernet_read).unwrap();
        }
    }

    fn tx(&mut self) {
        let mut a = Vec::new();

        for _ in 0..self.send_buffer.len() {
            a.push(self.send_buffer.pop());
        }

        //let mut a: [u8;8] = [0;8];
        //a[0] = self.send_buffer.pop();
        //a[1] = self.send_buffer.pop();
        //a[2] = self.send_buffer.pop();
        //a[3] = self.send_buffer.pop();

        if let Some(ref mut writer) = self.stream {
            for _ in 0..a.len() {
                writer.write(&a).expect("Failed to write to ip address!");
            }
        }

        println!("");
    }
}