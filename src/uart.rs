extern crate generic_array;

use crate::communication_interface::*;
use crate::protocol_interface::*;
use crate::ring_buffer::RingBuffer;
use crate::ring_buffer::RingBufferInterface;
use generic_array::ArrayLength;

pub struct UART<N, T> where N: ArrayLength<u8>, T: ProtocolInterface<N> {
    send_buffer: RingBuffer<N>,
    //receive_buffer: RingBuffer<N>, 
    protocol_type: T
}

impl<N, T> CommunicationInterface<N, T> for UART<N, T> where N: ArrayLength<u8>, T: ProtocolInterface<N>
{
    fn new() -> Self {
        UART { send_buffer: RingBufferInterface::new(), protocol_type: T::new() }
    }

    fn send(& mut self, mut p: T) -> bool {
        println!("UART::Send()");
        let buffer : &mut RingBuffer<N> = p.serialize();
        println!("Buffer length: {}", buffer.len());
        for _val in 0..buffer.len() {
            let a = buffer.pop();
            println!("Adding to UART send buffer: {}", a);
            self.send_buffer.push(a);
        }
        true
    }

    fn receive(& mut self) -> T {
        let a : T = T::new();
        a
    }

    /*fn rx(& mut self) {
        
    }

    fn tx(& mut self) {
        for _val in 0..self.send_buffer.len() {
            print!("{} ", self.send_buffer.pop());
        }
        println!("");
    }*/
}
