extern crate generic_array;

use crate::protocol_interface::ProtocolInterface;

use generic_array::ArrayLength;

/// # CommunicationInterface<N, T>
/// This is the basic functionality that any communication implementation needs to provide.
/// It's supposed to be hardware independent and is meant to be used at a higher-level.  
/// 
/// * `new()` - Creates a new instance of this communication interface.  
/// * `send(&mut self, p: T)` - Use to send a message. <T> is the ProtocolInterface this instance is using.  
/// * `recieve(&mut self)` - Use to recieve a message. 
///
/// ## Examples
/// An Ethernet interface will have to provide functions to send and recieve data to the user, without
/// any consideration to the harware specific implmentation. This stays the same for any Ethernet interfaces
/// no matter the architecture its built for (x86_64 or ARM etc.). It will make it easier to port the same
/// implementation to different architectures as only the internal hardware specific tx() and rx() needs to
/// be changed.  
/// 
/// An Ethernet implementation of the CommunicationInterface could look something like the following:
/// 
/// ```
/// impl<N, T> CommunicationInterface<N, T> for Ethernet<N, T> 
///     where N: ArrayLength<u8>, 
///     T: ProtocolInterface<N>
// {
///     fn new() -> Self {
///         Ethernet {/*settings*/} // initialize a new Ethernet connection
///     }
///     fn send(& mut self, mut p: T) -> bool {
///         let msg = p.serialize(); // serialize the protocol
///         self.send_buffer.push(msg); // add message to internal send buffer
///         tx(); // hardware specific transmission of message
///     }
///
///     fn receive(& mut self) -> T {
///         rx() // hardware specific recieve of message
///     }
/// }
/// ```
/// 
pub trait CommunicationInterface<N, T> where N: ArrayLength<u8>, T: ProtocolInterface<N> {
    /// Creates a new instance of this communication interface.  
    fn new() -> Self;
    /// Use to send a message. p: <T> is the protocol this instance is using which <T> is the type of protocol.  
    fn send(& mut self, p: T) -> bool;
    /// Use to recieve a message of type <T>. 
    fn receive(& mut self) -> T;
}