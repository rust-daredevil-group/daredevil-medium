extern crate generic_array;
extern crate heapless;

use crate::ring_buffer::RingBuffer;
use crate::ring_buffer::RingBufferInterface;
use crate::protocol_interface::ProtocolInterface;
use generic_array::ArrayLength;

pub struct CanMessage<N> where N: ArrayLength<u8> {
    data: RingBuffer<N>,
}

pub trait CanMessageTrait {
    fn new(s : &str) -> Self;
}

impl<N> ProtocolInterface<N> for CanMessage<N> where N: ArrayLength<u8> {
    fn new() -> Self {
        CanMessage { data: RingBufferInterface::new() }
    }
    fn serialize(&mut self) -> &mut RingBuffer<N> {
        &mut self.data
    }
    fn deseralize(&self, _v: RingBuffer<N>) {
        
    }
}

impl<N> CanMessageTrait for CanMessage<N> where N: ArrayLength<u8> {
    fn new(s : &str) -> Self {
        let mut ret : CanMessage<N> = CanMessage { data: RingBufferInterface::new() };
        
        let ch = s.chars();

        for element in ch {
            ret.data.push(element as u8);
            println!("Adding char: {}", element);
        }

        ret
    }
}