extern crate generic_array;
extern crate heapless;

use crate::ring_buffer::RingBuffer;
use crate::ring_buffer::RingBufferInterface;
use crate::protocol_interface::ProtocolInterface;
use generic_array::ArrayLength;

pub struct UartSimpleMessage<N> where N: ArrayLength<u8> {
    data: RingBuffer<N>,
}

pub trait UartSimpleMessageTrait<N> where N: ArrayLength<u8>{
    fn new(s : &str) -> Self;
    fn new_sensors(array : &[u8]) -> Self;
    fn get_data(&mut self) -> &mut RingBuffer<N>;
}

impl<N> ProtocolInterface<N> for UartSimpleMessage<N> where N: ArrayLength<u8> {
    fn new() -> Self {
        UartSimpleMessage { data: RingBufferInterface::new() }
    }
    fn serialize(&mut self) -> &mut RingBuffer<N> {
        &mut self.data
    }
    fn deseralize(&mut self, r_buf: &mut RingBuffer<N>) {
        for _ in 0..r_buf.len() {
           self.data.push(r_buf.pop());
        }
    }
}

impl<N> UartSimpleMessageTrait<N> for UartSimpleMessage<N> where N: ArrayLength<u8> {
    fn new(s : &str) -> Self {
        let mut ret : UartSimpleMessage<N> = UartSimpleMessage { data: RingBufferInterface::new() };
        
        let ch = s.chars();

        for element in ch {
            ret.data.push(element as u8);
            println!("Adding char: {}", element);
        }

        ret
    }

    fn new_sensors(array : &[u8]) -> Self {
        let mut ret : UartSimpleMessage<N> = UartSimpleMessage { data: RingBufferInterface::new() };

        for i in 0..8 {
            ret.data.push(array[i]);
        }
        
        ret
    }

    fn get_data(&mut self) -> &mut RingBuffer<N> {
        &mut self.data
    }

}
