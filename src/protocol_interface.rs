extern crate generic_array;

use crate::ring_buffer::RingBuffer;
use generic_array::ArrayLength;

/// ## ProtocolInterface<N>
/// This is the basic functionality any message protocol needs to implement. The underlying implementation 
/// can of course vary, i.e. what type of data to serialize and what output to deserialize.
/// 
/// * `new()` - Creates a new instance of this protocol.  
/// * `serialize(&mut self) -> &mut RingBuffer<N>` - Serializes the data and returns a reference to a RingBuffer with it.  
/// * `deserialize(&mut self, r_buf: &mut RingBuffer<N>)` - Deserializes the RingBuffer **r_buf**.
/// 
/// ## Examples
/// To send a message over Ethernet, the message first has to be serialized to bytes. Likewise to read a message from Ethernet
/// it needs to be deserialized to the desired out as we might not want to read the message in bytes. So we can create a protocol for that.
/// A simple message protocol EthernetMessage<N> that can be used for Ethernet coud look like the following:  
/// 
/// ```
/// impl<N> ProtocolInterface<N> for EthernetMessage<N> where N: ArrayLength<u8> {
///     fn new() -> Self {
///         EthernetMessage { data: String::new() } // Create an emtpy String to use for messages
///     }
///
///     fn serialize(&mut self) -> &mut RingBuffer<N> {
///         let length = self.msg.len();         
///         let mut serialized = self.msg.as_bytes();
///         let mut bfr: RingBuffer<N> = RingBufferInterface::new(); // Serialized data is stored in a RingBuffer
///         for i in 0..length {
///             bfr.push(serialized[i]);
///         }
///         &mut bfr
///     }
/// 
///     fn deseralize(&mut self, r_buf: &mut RingBuffer<N>) {
///         for _ in 0..r_buf.len() { // Deserializes the input r_buf, pushes each value to the String.
///            self.data.push(r_buf.pop());
///         }
///     }
/// }
/// ```

pub trait ProtocolInterface<N> where N: ArrayLength<u8> {
    /// Creates a new instance of this protocol interface. 
    fn new() -> Self;
    /// Serializes the data and returns a reference to a RingBuffer with it.  
    fn serialize(&mut self) -> &mut RingBuffer<N>;
    /// Deserializes the RingBuffer **r_buf**.
    fn deseralize(&mut self, r_buf: &mut RingBuffer<N>);
}