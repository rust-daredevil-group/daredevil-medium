extern crate typenum;

use generic_array::ArrayLength;
use generic_array::GenericArray;


/// # RingBuffer<N>
/// Simple implementation of a ring buffer in Rust. 
/// Stores values in a u8 array of size <N>.  
#[derive(Debug, Clone)]
pub struct RingBuffer<N> where N: ArrayLength<u8> {  
    buffer : GenericArray<u8,N>,
    head: u8,   // Where to pop from
    tail: u8,   // Where to push to
    len: u8,
}

pub trait RingBufferInterface<N> where N: ArrayLength<u8> {
    /// Creates a new empty RingBuffer.
    fn new() -> Self;
    /// Pushes a u8 value to the ring buffer.
    fn push(& mut self, val: u8);
    /// Pops a u8 value from the ring buffer.
    fn pop(& mut self) -> u8;
    /// Returns the maximum capacity of the ring buffer.
    fn capacity(&self) -> u8;
    fn len(&self) -> u8;
}

impl<N> RingBufferInterface<N> for RingBuffer<N> where N: ArrayLength<u8> { 
    fn new() -> Self {
        RingBuffer { buffer: GenericArray::<u8, N>::default(), head: 0, tail: 0, len: 0}
    }

    fn push(& mut self, val: u8) {
        //Are we full?
        if self.len == N::to_u8() {
            println!("Buffer full, nothing added! Length: {}, Capacity: {}", self.len, N::to_u8());
            return
        } 

        self.buffer[self.head as usize] = val;
        self.len = self.len + 1;
        self.head += 1;

        if self.head > N::to_u8() - 1 {
            self.head = 0;
        }
  
    }

    fn pop(& mut self) -> u8 {

        if self.len == 0 {
            println!("Nothing to read!");
            return 0
        }

        let val = self.buffer[self.tail as usize];
        self.len = self.len - 1;
        self.tail += 1;

        if self.tail > N::to_u8() - 1 {
            self.tail = 0;
        }

        return val
    }

    fn capacity(&self) -> u8 {
        N::to_u8()
    }

    fn len(&self) -> u8 {
        self.len
    }
}