pub extern crate typenum;
extern crate canlib_rs;

mod communication_interface;
mod protocol_interface;
mod uart;
mod ethernet;
mod can;
mod uart_simple_message;
mod ring_buffer;

use typenum::{U128};
use crate::communication_interface::CommunicationInterface;
use crate::ethernet::{Ethernet, EthernetTrait};
use crate::can::{Can, CanTrait};
use crate::uart_simple_message::{UartSimpleMessage, UartSimpleMessageTrait};
use crate::canlib_rs::*;
use crate::ring_buffer::RingBufferInterface;
use std::{thread::sleep,time::Duration};

use aes_frast::{aes_core, aes_with_operation_mode};
use aes::Aes128;
use cmac::{Cmac, Mac};

fn main() {
    
    // Init CANlib for Kvaser hw
    can_initialize_library(); 

    // Create new communications
    let mut visualizer : Ethernet<U128, UartSimpleMessage<U128>> = Ethernet::new();
    let mut can_ch0 : Can<U128, UartSimpleMessage<U128>> = Can::new();
    //let ip_addr = String::from("130.240.200.93:7777");
    let ip_addr = String::from("127.0.0.1:7777");
    let running = true;

    // Prepare CAN bus settings
    let can_flags = CanOpen::CanFd as i32 | CanOpen::Exclusive as i32 | CanOpen::RequireExtended as i32; // Set channel flags
    let ch0_settings = can::CanBusSettings{ // Create CAN bus settings
        channel: 0, 
        driver_type: CanDriver::Normal, 
        freq: CanBitrate::_500K, 
        tseg1: 64, 
        tseg2: 16, 
        sjw: 16, 
        no_samp: 0,
        can_fd: true,
        flags: can_flags,
        freq_brs: CanBitrate::Fd2M,
        tseg1_brs: 15,
        tseg2_brs: 4,
        sjw_brs: 4,
    };

    // Initialize communications
    can_ch0.init(ch0_settings);
    visualizer.init_stream(ip_addr);

    while running {
        println!("Looping");
        let dur : std::time::Duration = Duration::new(0, 5000000); // sleeps because we don't want to read too often
        sleep(dur);
        
        // Get CAN FD frames
        can_ch0.rx(); // Listening on CAN bus
        let mut buffer = can_ch0.receive(); // Receive the data from CAN
        let ring_bfr = buffer.get_data(); // Get a RingBuffer of the data
        let mut frame = [0u8; 64];
        
        // If CAN FD frames was received
        if ring_bfr.len() > 63 {
            for i in 0..64 {
                frame[i] = ring_bfr.pop();
            }
    
            // CAN FD frame slices
            // - frame[0..16]: message authentication code;
            // - frame[16..32]: initialization vector for encrypted data;
            // - frame[32..48]: encrypted sensor data, and
            // - frame[48..64]: unused, always 0.
            let mac = &frame[0..16];
            let iv = &frame[16..32];
            let enc_data = &frame[32..48];
            let _unused = &frame[48..64];
            let key = [0x2b,0x7e,0x15,0x16,0x28,0xae,0xd2,0xa6,0xab,0xf7,0x15,0x88,0x09,0xcf,0x4f,0x3c]; // use same keys as on the light module
            
            // Sensor data
            // - frame[32..34]: sensor index 0;
            // - frame[34..36]: sensor index 1;
            // - frame[36..38]: sensor index 2, and
            // - frame[38..40]: sensor index 3.
            if verify_message(mac, &frame[16..48], &key) {
                let sensor_data = decrypt_sensor_data(enc_data,iv,&key);
                let sensor_one = ((sensor_data[0] as u16) << 8) | sensor_data[1] as u16;
                let sensor_two = ((sensor_data[2] as u16) << 8) | sensor_data[3] as u16;
                let sensor_three = ((sensor_data[4] as u16) << 8) | sensor_data[5] as u16;
                let sensor_four = ((sensor_data[6] as u16) << 8) | sensor_data[7] as u16;
                print!("Sensor 1: {}\nSensor 2: {}\nSensor 3: {}\nSensor 4: {}\n",sensor_one,sensor_two,sensor_three,sensor_four);
                send_u8_array(&sensor_data[0..8], &mut visualizer);
            } else {
                println!("Error: Couldn't verify message!");
            }
        }

    }
    
    println!("Done");
    can_ch0.terminate();
    can_unload_library();
    println!("Bus terminated!");
    
}

// Send str through Ethernet
/*fn send_str(s : &str, e: &mut Ethernet<U128, UartSimpleMessage<U128>>) {
    println!("{}", s);
    let msg1 : UartSimpleMessage<U128> = UartSimpleMessageTrait::new(s);
    e.send(msg1);
    e.tx();
}*/

// Sends array of u8 through Ethernet
fn send_u8_array(array: &[u8], e: &mut Ethernet<U128, UartSimpleMessage<U128>>) {
    let msg : UartSimpleMessage<U128> = UartSimpleMessageTrait::new_sensors(array);
    e.send(msg);
    e.tx();
}

// Decrypts sensor data from light module. 
// * `input` : decrypted data (16 bytes)
// * `iv` : initialization vector (16 bytes)
// * `plainkey` : key to use (16 bytes)
fn decrypt_sensor_data(input: &[u8], iv: &[u8], plainkey: &[u8]) -> [u8; 16] {
    assert!(input.len() == 16);
    assert!(iv.len() == 16);
    assert!(plainkey.len() == 16);

    let mut w_keys: Vec<u32> = vec![0u32; 44]; // why 44?
    let mut output = [0u8; 16];

    aes_core::setkey_dec_auto(plainkey, &mut w_keys);

    aes_with_operation_mode::cbc_dec(input, &mut output, &w_keys, iv);

    output
}

// Verify message integrity from the light module.
// * `cmac` : message authentication code (16B)
// * `msg` : decrypted data (16B) + iv (16B)
// * `plainkey` : key to use (16B)
fn verify_message(cmac: &[u8], msg: &[u8], plainkey: &[u8]) -> bool {
    assert!(cmac.len() == 16);
    assert!(msg.len() == 32);
    assert!(plainkey.len() == 16);

    let mut mac = Cmac::<Aes128>::new_varkey(plainkey).unwrap();
    mac.input(msg);
    match mac.verify(cmac){
        Ok(_) => true,
        Err(_) => false,
    }
}
